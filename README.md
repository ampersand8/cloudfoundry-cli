# cloudfoundry-cli
A simple alpine docker image with the Cloud Foundry cli v6 and v7 (beta) (https://github.com/cloudfoundry/cli) installed.

To use cf v7 you can use `cf7` or `cf`. To use cf v6 you can use `cf6`.

# Build
```bash
docker build --tag cloudfoundry-cli .
```

# Usage
## Interactive
You can use the shell and use it interactively.
```bash
docker run -it --rm cloudfoundry-cli /bin/sh
> cf version
> cf7 version
> cf6 version
```

## Execute command
You can also simply run a command
```bash
docker run --rm cloudfoundry-cli cf version
docker run --rm cloudfoundry-cli cf7 version
docker run --rm cloudfoundry-cli cf6 version
```