FROM alpine

# Install cf cli and jq
RUN apk --update --virtual .build-deps add --no-cache curl && \
apk --no-cache add ca-certificates jq && \
update-ca-certificates && \
curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -zx && \
mv cf /usr/local/bin/cf6 && \
curl -L "https://packages.cloudfoundry.org/stable?release=linux64-binary&version=v7&source=github" | tar -zx && \
mv cf7 /usr/local/bin && \
mv cf /usr/local/bin && \
apk del .build-deps